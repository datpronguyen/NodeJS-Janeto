import express from 'express'
import { readFile } from './file'

const app = new express();
const port = 3000

app.get('/', (req, res) => res.send('Hello World!'))

app.get('/users', (req, res) => {
  readFile('./user.json').then(data => {
    return res.status(200).send(JSON.parse(data))
  }).catch(err => {
    return res.status(404).send(err)
  })
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))