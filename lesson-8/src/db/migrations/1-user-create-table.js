module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('User', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      username: Sequelize.STRING,
      password: {
        type: Sequelize.STRING,
        set: function(val) {
          if (!val) {
            return
          }
          const hash = bcrypt.hashSync(val, 9)
          this.setDataValue('password', hash)
        }
      },
      role: Sequelize.STRING
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('User')
  }
}
